//
//  River.swift
//  River
//
//  Created by Neal Watkins on 2020/3/19.
//  Copyright © 2020 Waiata. All rights reserved.
//

import SceneKit

class River {
    
    var dealer = Deck.full52
    
    var steps = [Step]()
    
    var lights = [Light]()
    
    var shots: Int {
        return table?.drinkNode?.childNodes.count ?? 0
    }
    
    weak var table: Table?
    
    /// how easy the game is
    var deckLevel = Default.startDeck {
        didSet {
            if deckLevel < 1 { deckLevel = 1 }
        }
    }
    
    func add(step: Step) {
        steps.append(step)
    }
    
    func add(light: Light) {
        lights.append(light)
    }
    
    /// start over
    func redeal() {
        clearSteps()
        deckLevel = Default.startDeck + 1
        newDeck()
        dealFix()
    }
    
    /// run out of cards so get more (make it easier to win this time)
    func newDeck() {
        dealer = Deck.up(from: deckLevel)
        deckLevel -= 1
        table?.render()
        dealer.shuffle()
        dealer.audio()
    }
    
    /// empty the table
    func clearSteps() {
        for step in steps {
            step.clear()
        }
        light()
    }
    
    /// update steps so there's playable cards everyehere there should be
    func dealFix() {
        for step in steps {
            if !step.isPassed {
                deal(to: step)
            }
        }
        light()
    }
    
    /// fill the table (cover all steps)
    func dealAll() {
        for step in steps {
            if step.isOpen {
                deal(to: step)
            }
        }
        light()
    }
    
   
    
    /// deal individual card
    func deal(to step: Step) {
        if dealer.isEmpty {
            newDeck()
        }
        dealer.deal(to: step)
    }
    
    
    /// how far along are we
    var currentProgress: CGFloat {
        return steps.filter{ $0.isPassed }.map{ $0.progress }.max() ?? 0
    }
    
    var nextProgress: CGFloat {
        let current = currentProgress
        return steps.filter{ $0.progress > current}.map{ $0.progress }.min() ?? 0
    }
    
    var finishProgress: CGFloat {
        return steps.map{ $0.progress }.max() ?? 0
    }
    
    
    /// give up and see what I was up against
    func revealAll() {
        for step in steps {
            if !step.isPassed {
                step.topCard?.reveal()
            }
        }
        light()
    }
    
    /// turn over a card (if allowed)
    func reveal(_ card: Card) {
        guard let step = card.pile as? Step else { // rouge card - just flip it
            card.reveal()
            return
        }
        if step.progress == nextProgress { // only allow next row of cards
            card.reveal()
            if card.isPicture {
                ohoh()
            } else {
                success()
            }
        }
        light()
    }
    
    /// adjust lighting
    func light() {
        let nextProgress = self.nextProgress
        var next = steps.filter{ $0.progress == nextProgress}
        var past = steps.filter{ $0.isPassed }
        var drinkTarget = table?.drinkNode
        for light in lights {
            if let drink = drinkTarget, self.shots > 0, currentProgress < finishProgress {
                drinkTarget = nil
                light.doing = .full(drink)
            } else if let step = next.first {
                next.removeFirst()
                light.doing = .next(step.node)
            } else if let step = past.first {
                past.removeFirst()
                light.doing = .past(step.node)
            } else  {
                light.doing = .nothing
            }
        }
    }
    
    
    /// hit a picture card - go back to the start
    func ohoh() {
        drink()
        dealAll()
    }
    
    /// successfully turned over a card - move on
    func success() {
        if currentProgress >= finishProgress {
            win()
        }
    }
    
    /// turned over a picture card - start again
    func drink() {
        guard let shot = SCNScene(named: "Shot.scn") else { return }
        table?.have(drink: shot)
        table?.render()
    }
    
    func win() {
        table?.clearDrinks()
    }
    
    
    var maxZ: CGFloat {
        return steps.filter{ $0.isPassed }.map{ $0.progress }.max() ?? 0
    }
    
    var nextZ: CGFloat {
        let max = maxZ
        return steps.map{ $0.progress }.filter{ $0 > max }.min() ?? 0
    }
    
    struct Default {
        static let defaults = UserDefaults.standard
        static var startDeck: Int {
            return defaults.integer(forKey: "StartDeck")
        }
    }
    
}
