//
//  Light.swift
//  River
//
//  Created by Neal Watkins on 2020/3/25.
//  Copyright © 2020 Waiata. All rights reserved.
//

import SceneKit

class Light {
    
    var doing = Doing.nothing {
        didSet {
            render()
        }
    }
    
    var node: SCNNode
    
    var light: SCNLight? {
        return node.light
    }
    
    init?(node: SCNNode) {
        guard let _ = node.light else { return nil }
        self.node = node
    }
    
    func render() {
        SCNTransaction.animationDuration = 1.0
        light?.intensity = doing.intensity
        node.constraints = doing.constraints
    }
    
    enum Doing {
        case nothing
        case next(SCNNode)
        case past(SCNNode)
        case full(SCNNode)
        
        var intensity: CGFloat {
            switch self {
            case .nothing: return 0
            case .past: return 1000
            case .next: return 2000
            case .full: return 1000
            }
        }
        
        var constraints: [SCNConstraint] {
            switch self {
            case .nothing: return []
            case .past(let node): return [SCNLookAtConstraint(target: node)]
            case .next(let node): return [SCNLookAtConstraint(target: node)]
            case .full(let node): return [SCNLookAtConstraint(target: node)]
            }
        }

    }
}
