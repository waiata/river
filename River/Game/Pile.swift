//
//  Pile.swift
//  River
//
//  Created by Neal Watkins on 2020/3/20.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Foundation
import SceneKit

protocol Pile: AnyObject {
    
    var cards: [Card] { get set }
    var node: SCNNode { get set }
    
    func draw(card: Card?)
    
}

extension Pile {
    
    func deal(_ cards: Int = 1, from: From = .top, to: Pile) {
        let destination = to
        for _ in 1...cards {
            let drawn = card(from: from)
            destination.draw(card: drawn)
        }
    }
    
    func card(from: From) -> Card? {
        if cards.isEmpty { return nil }
        switch from {
        case .top: return cards.removeFirst()
        case .bottom: return cards.removeLast()
        case .random: let pick = cards.indices.randomElement()!
            return cards.remove(at: pick)
        }
    }
    
    func draw(cards: [Card]) {
        for card in cards {
            draw(card: card)
        }
    }
    
    func draw(card: Card?) {
        guard let drawn = card else { return }
        drawn.pile = self
        cards.append(drawn)
        node.addChildNode(drawn)
    }
    
    func empty() {
        cards = []
        for child in node.childNodes {
            child.removeFromParentNode()
        }
    }
    
    func shuffle() {
        cards = cards.shuffled()
    }
    
    var isEmpty: Bool {
        return cards.isEmpty
    }
    
    var topCard: Card? {
        return cards.last
    }
}

enum From {
    case top
    case bottom
    case random
}
