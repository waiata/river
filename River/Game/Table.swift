//
//  Table.swift
//  River
//
//  Created by Neal Watkins on 2020/3/23.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Cocoa
import SceneKit

class Table: SCNView {

    
    @IBAction func newGame(_ sender: Any) {
        clearDrinks()
        river.redeal()
    }
    
    @IBAction func clearTable(_ sender: Any) {
        river.clearSteps()
    }
    
    @IBAction func nextDeck(_ sender: Any) {
        river.newDeck()
    }
    
    @IBAction func deal(_ sender: Any) {
        river.dealFix()
    }
    
    @IBAction func reveal(_ sender: Any) {
        river.revealAll()
    }
    
    var river = River()
    
    /// set up scene
    func build() {
        attachSteps()
        attachlights()
        river.table = self
        allowsCameraControl = true
    }
    
    /// update
    func render() {
        let deckLevel = "\(river.deckLevel)+"
        deckLabel?.string = deckLevel
        drinkLabel?.string = "\(river.shots)"
    }
    
    /// add a drink node
    func have(drink: SCNScene) {
        let start = drinkNode
        let shot = drink.rootNode
        start?.addChildNode(shot)
        slide(drink: shot)
    }
    
    func slide(drink: SCNNode) {
        let push = SCNVector3(10000, 0, 100)
        drink.physicsBody?.applyForce(push, asImpulse: true)
    }
    
    func clearDrinks() {
        guard let shots = drinkNode?.childNodes.reversed().enumerated() else { return }
        for (i,shot) in shots {
            flyAway(shot, delay: i)
        }
    }
    
    func flyAway(_ node: SCNNode, delay: Int = 0) {
          node.physicsBody = .none
          let wait = SCNAction.wait(duration: Double(delay) * Default.flyDelay)
          let fly = SCNAction.move(by: Default.flight, duration: Default.flyTime)
          let action = SCNAction.sequence([wait, fly])
          node.runAction(action) {
              node.removeFromParentNode()
          }
      }
      
    
    /// make steps part of the scene
    func attachSteps() {
        guard let steps = scene?.rootNode.childNode(withName: Table.stepRoot, recursively: false)  else { return }
        var bit = 2
        for node in steps.childNodes {
            let step = Step(node: node)
//            step.bit(mask: bit)
            bit = bit << 1
            river.add(step: step)
        }
    }
    
    
    /// find all the lights and add them
    func attachlights() {
        guard let lights = scene?.rootNode.childNode(withName: Table.lightRoot, recursively: false)  else { return }
        
        for node in lights.childNodes {
            guard let light = Light(node: node) else { continue }
            river.add(light: light)
        }
    }
    
    
    //MARK: Nodes
    
    var drinkNode: SCNNode? {
        return  scene?.rootNode.childNode(withName: Table.drinkNode, recursively: true)
    }
    
    var drinkLabel: SCNText? {
        return  scene?.rootNode.childNode(withName: Table.drinkLabel, recursively: true)?.geometry as? SCNText
    }
    
    var deckLabel: SCNText? {
        return  scene?.rootNode.childNode(withName: Table.deckLabel, recursively: true)?.geometry as? SCNText
    }
    
    static let stepRoot = "Steps"
    static let lightRoot = "Lights"
    static let drinkNode = "Drinks"
    static let drinkLabel = "Drink Label"
    static let deckLabel = "Deck Label"
    
    //MARK: Defaults
    
    struct Default {
        static let defaults = UserDefaults.standard
        
        static var flight: SCNVector3 {
            let height = CGFloat(defaults.float(forKey: "CardFlyHeight"))
            return SCNVector3(x: 0, y: height, z: 0)
        }
        
        static var flyTime: TimeInterval {
            return defaults.double(forKey: "CardFlyTime")
        }
        
        static var flyDelay: TimeInterval {
            return defaults.double(forKey: "CardFlyDelay")
        }
        
    }
    
    
    //MARK: Events
    
    override func mouseDown(with event: NSEvent) {
        let loc = convert(event.locationInWindow, from: nil)
        let hit = hitTest(loc, options: nil)
        if let card = hit.first?.node as? Card {
            river.reveal(card)
        }
    }
}
