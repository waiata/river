//
//  Deck.swift
//  River
//
//  Created by Neal Watkins on 2020/3/19.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Foundation
import SceneKit

/// deck of cards
class Deck: Pile {
    
    var cards = [Card]()
    var node = SCNNode()
    
    static var full52: Deck {
        var deck = Deck()
        for suit in Card.Suit.all {
            for rank in Card.Rank.all {
                let card = Card(rank: rank, suit: suit)
                deck.draw(card: card)
            }
        }
        return deck
    }
    
    static func up(from: Int) -> Deck {
        var deck = Deck()
        for rank in Card.Rank.up(from: from) {
            for suit in Card.Suit.all {
                let card = Card(rank: rank, suit: suit)
                deck.draw(card: card)
            }
        }
        return deck
    }
    
    func audio() {
        let play = SCNAction.playAudio(Default.shuffleSound, waitForCompletion: false)
        node.runAction(play)
    }
    
    
    struct Default {
          static let defaults = UserDefaults.standard
          
          static var position: SCNVector3 {
              let x = CGFloat(defaults.float(forKey: "DealerX"))
              let y = CGFloat(defaults.float(forKey: "DealerY"))
              let z = CGFloat(defaults.float(forKey: "DealerZ"))
            return SCNVector3(x: x, y: y, z: z)
          }
        
        static var shuffleSound: SCNAudioSource {
            let name = defaults.string(forKey: "DeckShuffleSound") ?? ""
            let sound = SCNAudioSource(fileNamed: name)
            return sound ?? SCNAudioSource()
        }
          
      }
    
}
