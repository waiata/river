//
//  Card.swift
//  River
//
//  Created by Neal Watkins on 2020/3/19.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Cocoa
import SceneKit

class Card: SCNNode {
    
    //MARK: Parameters
    var rank = Rank.joker
    var suit = Suit.none
    var face = Face.down
    weak var pile: Pile?
    
    
    //MARK: Maths
    
    var isPicture: Bool {
        return Rank.picture.contains(self.rank)
    }
    
    //MARK: Build
    convenience init(rank: Rank, suit: Suit) {
        self.init()
        self.rank = rank
        self.suit = suit
        build()
    }
    
    func build() {
        let geometry = SCNShape(path: path, extrusionDepth: Default.depth)
        geometry.materials = [backMaterial(), faceMaterial(), extrusionMaterial()]
        self.geometry = geometry
        self.physicsBody = .dynamic()
    }
    
    var path: NSBezierPath {
        let x = Default.width
        let y = Default.height
        let rect = CGRect(x: 0 - x / 2 , y: 0 - y / 2, width: x, height: y)
        let radius = Default.corners
        let path = NSBezierPath(roundedRect: rect, xRadius: radius, yRadius: radius)
        path.close()
        path.flatness = radius / 20
        return path
    }
    
    
    //MARK: Materials
    
    func layer() -> CALayer {
        let layer = CALayer()
        let size = Default.pixelSize
        layer.backgroundColor = .white
        layer.contents = rank.image
        layer.frame = CGRect(origin: .zero, size: size)
        let pitch = CGSize(width: size.width / 10, height: size.height / 10)
        for pip in rank.pips {
            let lay = suit.layer()
            lay.position = CGPoint(x: pip.0 * pitch.width, y: pip.1 * pitch.height)
            if pip.1 < 5 { // rotate bottom half
                
            }
            layer.addSublayer(lay)
        }
        return layer
    }
    
    func faceMaterial() -> SCNMaterial {
        let material = SCNMaterial()
        material.diffuse.contents = layer()
        return material
    }
    
    func backMaterial() -> SCNMaterial {
        let material = SCNMaterial()
        material.diffuse.contents = #imageLiteral(resourceName: "Back Pattern")
        return material
    }
    
    func extrusionMaterial() -> SCNMaterial {
        let material = SCNMaterial()
        material.diffuse.contents = CGColor.white
        return material
    }
    
    //MARK: Play
    /// if not already flipped over then do it
    func reveal() {
        if face == .down {
            flip()
        } else {
            
        }
    }
    
    //MARK: Animate
    
    func flyAway(delay: Int) {
        self.physicsBody = .none
        let wait = SCNAction.wait(duration: Double(delay) * Default.flyDelay)
        let fly = SCNAction.move(by: Default.flight, duration: Default.flyTime)
        let play = SCNAction.playAudio(Default.flySound, waitForCompletion: false)
        let away = SCNAction.group([fly, play])
        let action = SCNAction.sequence([wait, away])
        self.runAction(action) {
            self.removeFromParentNode()
        }
    }
    
    func flip() {
        face = face.flipped
        let jump = SCNAction.move(by: Default.jump, duration: 0.25)
        let flip = SCNAction.rotate(by: .pi, around: SCNVector3(x: 0, y: 1, z: 0), duration: 0.5)
        let play = SCNAction.playAudio(rank.sound, waitForCompletion: false)
        let action = SCNAction.group([jump, flip, play])
        self.runAction(action)
    }
    
    
    //MARK: Suit
    
    enum Suit: String {
        case spades = "♠︎"
        case hearts = "♥︎"
        case clubs = "♣︎"
        case diamonds = "♦︎"
        case none = ""
        
        static let all: [Suit] = [.spades, .hearts, .clubs, .diamonds]
        static let red: [Suit] = [.hearts, .diamonds]
        static let black: [Suit] = [.spades, .clubs]
        
        var color: CGColor {
            if Suit.red.contains(self) { return CGColor(red: 1, green: 0, blue: 0, alpha: 1)}
            else { return CGColor.black }
        }
        
        func layer(size: CGSize = .zero) -> CALayer {
            let pip = CALayer()
            pip.bounds.size = Default.pipSize
            pip.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            switch self {
            case .spades: pip.contents = NSImage(named: "spade")
            case .hearts: pip.contents = NSImage(named: "heart")
            case .clubs: pip.contents = NSImage(named: "club")
            case .diamonds: pip.contents = NSImage(named: "diamond")
            case .none: break
            }
            return pip
        }
        
    }
    
    //MARK: Rank
    
    enum Rank: Int {
        case joker = 0
        case ace = 1
        case deuce = 2
        case three = 3
        case four = 4
        case five = 5
        case six = 6
        case seven = 7
        case eight = 8
        case nine = 9
        case ten = 10
        case jack = 11
        case queen = 12
        case king = 13
        
        static let all: [Rank] = [.ace, .deuce, .three, .four, .five, .six, .seven, .eight, .nine, .ten, .jack, .queen, .king]
        static let picture: [Rank] = [.jack, .queen, .king]
        
        static func up(from: Int) -> [Rank] {
            return all.filter { $0.rawValue >= from }
        }
        
        var pips: [(CGFloat,CGFloat)] {
            switch self {
            case .ace:      return [(5,5)]
            case .deuce:    return [(5,2), (5,8)]
            case .three:    return [(5,2), (5,5), (5,8)]
            case .four:     return [(2,2), (8,2), (2,8), (8,8)]
            case .five:     return [(2,2), (8,2), (5,5), (2,8), (8,8)]
            case .six:      return [(2,2), (8,2), (2,5), (8,5), (2,8), (8,8)]
            case .seven:    return [(2,2), (8,2), (2,5), (8,5), (5,6), (2,8), (8,8)]
            case .eight:    return [(2,2), (8,2), (2,4), (8,4), (2,6), (8,6), (2,8), (8,8)]
            case .nine:     return [(2,2), (8,2), (2,4), (8,4), (5,5), (2,6), (8,6), (2,8), (8,8)]
            case .ten:      return [(2,2), (8,2), (5,3), (2,4), (8,4), (2,6), (8,6), (5,7), (2,8), (8,8)]
            @unknown default:
                return [(1,1), (9,1), (1,9), (9,9)]
            }
        }
        
        var image: NSImage? {
            switch self {
                case .king : return NSImage(named: "King")
                case .queen : return NSImage(named: "Queen")
                case .jack : return NSImage(named: "Jack")
                default : return nil
            }
        }
        
        var sound: SCNAudioSource {
            if Rank.picture.contains(self) {
                return Default.PictureCardSound
            } else {
                return Default.RankCardSound
            }
        }
    }
    
    //MARK: Face
    
    enum Face: String {
        case down = "🂠"
        case up = "🃏"
        case none = "."
        
        var flipped: Face {
            switch self {
            case .down : return .up
            case .up : return .down
            default : return .up
            }
        }
    }
    
    
    //MARK: Defaults
    
    struct Default {
        static let defaults = UserDefaults.standard
        
        static var jump: SCNVector3 {
            let height = CGFloat(defaults.float(forKey: "CardJumpHeight"))
            return SCNVector3(x: 0, y: 0, z: height)
        }
        static var flight: SCNVector3 {
            let height = CGFloat(defaults.float(forKey: "CardFlyHeight"))
            return SCNVector3(x: 0, y: 0, z: height)
        }
        
        static var flyTime: TimeInterval {
            return defaults.double(forKey: "CardFlyTime")
        }
        
        static var flyDelay: TimeInterval {
            return defaults.double(forKey: "CardFlyDelay")
        }
        
        static var flySound: SCNAudioSource {
            let name = defaults.string(forKey: "CardFlySound") ?? ""
            let sound = SCNAudioSource(fileNamed: name)
            return sound ?? SCNAudioSource()
        }
        
        static var RankCardSound: SCNAudioSource {
            let name = defaults.string(forKey: "CardRankSound") ?? ""
            let sound = SCNAudioSource(fileNamed: name)
            return sound ?? SCNAudioSource()
        }
        
        static var PictureCardSound: SCNAudioSource {
            let name = defaults.string(forKey: "CardPictureSound") ?? ""
            let sound = SCNAudioSource(fileNamed: name)
            return sound ?? SCNAudioSource()
        }
        
        
        static var width: CGFloat {
            return CGFloat(defaults.float(forKey: "CardGeometryWidth"))
        }
        static var height: CGFloat {
            return CGFloat(defaults.float(forKey: "CardGeometryHeight"))
        }
        static var corners: CGFloat {
            return CGFloat(defaults.float(forKey: "CardGeometryCorners"))
        }
        static var depth: CGFloat {
            return CGFloat(defaults.float(forKey: "CardGeometryDepth"))
        }
        static var pixelSize: CGSize {
            let width = CGFloat(defaults.float(forKey: "CardPixelWidth"))
            let height = CGFloat(defaults.float(forKey: "CardPixelHeight"))
            return CGSize(width: width, height: height)
        }
        static var pipSize: CGSize {
            let width = CGFloat(defaults.float(forKey: "PipWidth"))
            let height = CGFloat(defaults.float(forKey: "PipHeight"))
            return CGSize(width: width, height: height)
        }
    }
    
}

extension Card {
    
    override var description: String {
        return "\(face.rawValue) \(rank.rawValue)\(suit.rawValue)"
    }
    
    func debugQuickLookObject() -> Any? {
        return layer()
    }
}
