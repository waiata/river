//
//  Step.swift
//  River
//
//  Created by Neal Watkins on 2020/3/20.
//  Copyright © 2020 Waiata. All rights reserved.
//

import SceneKit

class Step: Pile {
    
    var cards = [Card]()
    var node: SCNNode
    
    init(node: SCNNode) {
        self.node = node
    }
    
    /// is the top card revealed?
    var isOpen: Bool {
        guard let top = topCard else { return true }
        return top.face == .up
    }
    
    /// has this step been successfully negotiated
    var isPassed: Bool {
        guard let top = topCard else { return false } // no cards
        return top.face == .up && !top.isPicture
    }
    
    /// how far along this step is
    var progress: CGFloat {
        return node.position.z
    }
    
    /// get rid of all the cards
    func clear() {
        for (i,card) in cards.enumerated() {
            card.flyAway(delay: i)
        }
        cards = []
    }
    
    func draw(card: Card?) {
        guard let drawn = card else { return }
        node.addChildNode(drawn)
        drawn.pile = self
        cards.append(drawn)
        drawn.position = Default.dropFrom
        
    }
    
    struct Default {
        static let defaults = UserDefaults.standard
        static var dropFrom: SCNVector3 {
            let drop = CGFloat(defaults.float(forKey: "CardDropHeight"))
            return SCNVector3(x: 0, y: 0, z: drop)
        }
        
    }
    
    
}

extension Step: CustomStringConvertible {
    var description: String {
        var qwerty = "Step \(node.name ?? "")"
        if let top = topCard {
            qwerty += top.description
        }
        return qwerty
    }
}
