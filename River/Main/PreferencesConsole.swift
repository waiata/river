//
//  PreferencesConsole.swift
//  River
//
//  Created by Neal Watkins on 2020/3/26.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Cocoa

class PreferencesConsole: NSViewController {

    @IBOutlet weak var startDeckLabel: NSTextField!
    
    @IBAction func changeStartDeck(_ sender: Any) {
        render()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        render()
    }
    
    func render() {
        startDeckLabel.stringValue = "Start with Deck \(Default.startDeck)+"
    }
    
    struct Default {
        static let defaults = UserDefaults.standard
        
        static var startDeck: Int {
            return defaults.integer(forKey: "StartDeck")
        }
    }
}
