//
//  AppDelegate.swift
//  River
//
//  Created by Neal Watkins on 2020/3/19.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        registerDefaults()
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }

    func registerDefaults() {
        let plist = Bundle.main.path(forResource: "Defaults", ofType: "plist")
        if let dic = NSDictionary(contentsOfFile: plist!) as? [String: Any] {
            UserDefaults.standard.register(defaults: dic)
        }
    }
}

