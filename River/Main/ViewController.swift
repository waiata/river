//
//  ViewController.swift
//  River
//
//  Created by Neal Watkins on 2020/3/19.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {

    
    @IBOutlet weak var tableScene: Table!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableScene.build()
    }
    
   
}

